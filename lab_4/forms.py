from django import forms
from lab_2.models import Note
  
class NoteForm(forms.ModelForm):
    # specify the name of model to use
    class Meta:
        model = Note
        fields = "__all__"
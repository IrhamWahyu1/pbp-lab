1. Apakah perbedaan antara JSON dan XML?
2. Apakah perbedaan antara HTML dan XML?

Jawaban no 1.
JavaScript Object Notation atau JSON merupakan sebuah format penggunaan tipe data berupa angka dan teks dalam merepresentasikan objek untuk melakukan pertukaran data yang berbasis teks. Sementara itu Extensive Markup Language atau XML merupakan sebuah format penggunaan cara penulisan HTML untuk melakukan format data yang berbasis teks yang berasal dari SGML. Berdasarkan definsi baik dari JSON maupun XML, sudah terlihat jelas perbedaan dari keduanya. Selain itu juga XML memerlukan tag seperti HTML di awal dan akhir sedangkan JSON tidak memerlukan hal tersebut. Tujuan JSON juga untuk mempermudah pertukaran data dengan format file JSON yang ringan dan tidak rumit. Sementara itu tujuan dari XML singkatnya adalah sebagai format standar untuk menyandikan data yang dapat dibaca oleh user dan oleh mesin

Jawaban no 2.
Hypertext Markup Language atau HTML merupakan sebuah bahasa untuk membuat website dari strukturnya. Perbedaan antara XML dan HTML adalah HTML tidak bersifat case sensitive sementara XML bersifat case sensitive. Selain itu juga, HTML merujuk kepada pembuatan website sementara XML merujuk kepada format standar untuk menyandikan data yang dapat dibaca oleh user maupun oleh mesin. Kemudian perbedaan selanjutnya adalah tag pada XML dapat ditentukan oleh programmer sementara tag pada HTML sudah ditentukan sejak awal oleh HTML itu sendiri. Selain itu, HTML berfokus kepada penyajian dari data sementara XML berfokus pada pembawaan informasi


Referensi:
https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html

https://id.sawakinome.com/articles/protocols--formats/difference-between-json-and-xml-3.html